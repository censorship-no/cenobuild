#!/bin/bash
BUILD_ID=${BUILD_ID:-'android_components -l'}

if [[ $1 == '-i' ]]; then
    # Use interactive mode
    OPTIONAL_ARGS='-it --entrypoint /bin/bash'
fi

docker run \
    --rm \
    --user fdroid \
    --network host \
    -v $PWD:/home/fdroid/cenobuild \
    -e BUILD_ID="$BUILD_ID" \
    -e GROUP_ID="ie.equalit.ouinet" \
    -e OSSRH_USERNAME="$OSSRH_USERNAME" \
    -e OSSRH_PASSWORD="$OSSRH_PASSWORD" \
    -e SONATYPE_STAGING_PROFILE_ID="$SONATYPE_STAGING_PROFILE_ID" \
    -e SIGNING_KEY_ID="$SIGNING_KEY_ID" \
    -e SIGNING_PASSWORD="$SIGNING_PASSWORD" \
    -e SIGNING_KEY="$SIGNING_KEY" \
    ${OPTIONAL_ARGS} \
    cenodev/ceno-fdroid:latest