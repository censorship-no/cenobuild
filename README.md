What's this?
------------

This is a fork of Relan's excellent [fennecbuild repository](https://gitlab.com/relan/fennecbuild).
It allows compiling the Mozilla's GeckoView and Android-Components without prebuilts or proprietary blobs and with patches applied to allow them to be used for Ceno Browser. The binaries built with this repository are published here, https://repo.maven.apache.org/maven2/ie/equalit/ouinet/

This is compiled using fdroidserver.
You will need to copy the config files from -fdroiddata/ into their respective folders first.
Then run:
```
fdroid build ie.equalit.android_components:VERCODE
```
where VERCODE is a version code number of targeted release of Android-Components (e.g. v133.0.2 has the VERCODE 21330200).

Known Issues
------------
Please see the list of known issues and workarounds before opening an issue!
https://gitlab.com/censorship-no/ceno-browser/-/issues

Setting up the VM
-----------------
Getting a working fdroidserver is a bit tricky.
Here are some steps to get you in the right direction.
- Setup a VM, you'll want at least 16GB RAM and 64GB of storage
- Fedora 34 and Debian 10 have been tested to work
- `git clone https://gitlab.com/fdroid/fdroidserver.git`
- `git clone https://gitlab.com/fdroid/fdroiddata.git`
- Install JDK 8 and 11, set 11 as default
- Put this in your path as your gradle, make sure the folder above it is writable for its cache:
```
wget https://gitlab.com/fdroid/fdroidserver/-/raw/master/gradlew-fdroid -O gradle
```
- Disable the Gradle daemon to prevent OOM:
```
mkdir -p ~/.gradle && echo "org.gradle.daemon=false" >> ~/.gradle/gradle.properties
```
- setup Android SDK/NDK:
```
ANDROID_HOME="$USER_HOME/android-sdk-linux"
SDK_MAN="$ANDROID_HOME/cmdline-tools/bin/sdkmanager"
mkdir -p $ANDROID_HOME
wget https://dl.google.com/android/repository/commandlinetools-linux-6858069_latest.zip
unzip commandlinetools-linux-6858069_latest.zip -d $ANDROID_HOME
yes y | $SDK_MAN --sdk_root="$ANDROID_HOME" platform-tools "build-tools;30.0.3"
yes y | $SDK_MAN --sdk_root="$ANDROID_HOME" --install "ndk;21.3.6528147"
yes y | $SDK_MAN --sdk_root="$ANDROID_HOME" --install "ndk;25.0.8775105"
yes y | $SDK_MAN --sdk_root="$ANDROID_HOME" --licenses
echo "sdk_path: $ANDROID_HOME" >> $USER_HOME/fdroiddata/config.yml
```
- Add environment variables to your .bashrc
```
echo "ANDROID_HOME=$ANDROID_HOME" >> ~/.bashrc
echo "PATH=$PATH:$ANDROID_HOME/tools:$ANDROID_HOME/platform-tools:~/fdroidserver" >> ~/.bashrc
```
- `source .bashrc`
- Fedora:
```
sudo ln -sf /usr/lib/jvm/java-11-openjdk /usr/lib/jvm/java-11-openjdk-amd64
sudo ln -sf /usr/lib/jvm/java-1.8.0-openjdk /usr/lib/jvm/java-8-openjdk-amd64
```

Licenses
--------

The scripts are licensed under the GNU Affero General Public License version 3 or later.

Changes in the patch are licensed according to the header in the files this patch adds or modifies (Apache 2.0 or MPL 2.0).

The artwork is licensed under the MPL 2.0.
