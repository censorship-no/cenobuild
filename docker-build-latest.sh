#!/bin/bash
PREV_COMMIT=$(git rev-parse HEAD)
git pull origin acbuild >/dev/null 2>&1
CURRENT_COMMIT=$(git rev-parse HEAD)
if [ "${CURRENT_COMMIT}" != "${PREV_COMMIT}" ]; then
    echo "New commit applied to acbuild branch."
    echo "Rebuilding cenodev/ceno-fdroid docker image."
    docker build -t cenodev/ceno-fdroid - < Dockerfile
    echo "Building Android-Componets from cenobuild commit ${CURRENT_COMMIT}."
    ./docker-build.sh
fi