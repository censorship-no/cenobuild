#!/bin/bash
cp -rf ${CENOBUILD}/~fdroiddata/* .
BUILD_ARG=$([ -z "$BUILD_ID" ] && echo "ceno" || echo "${BUILD_ID}")
fdroid build -v ie.equalit.${BUILD_ARG}
