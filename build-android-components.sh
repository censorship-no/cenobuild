#!/bin/bash
#
#    Fennec build scripts
#    Copyright (C) 2020-2024  Matías Zúñiga, Andrew Nayenko, Tavi
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

set -e

if [ -z "$1" ] || [ -z "$2" ]; then
    echo "Usage: $0 versionName versionCode" >&1
    exit 1
fi

# shellcheck source=paths.sh
source "$(dirname "$0")/paths.sh"

# We publish the artifacts into a local Maven repository instead of using the
# auto-publication workflow because the latter does not work for Gradle
# plugins (Glean).

# Set up Android SDK
sdkmanager 'build-tools;34.0.0' # for GeckoView
sdkmanager 'ndk;25.2.9519653' # for GleanAS

# Set up Rust
"$rustup"/rustup-init.sh -y --no-update-default-toolchain
# shellcheck disable=SC1090,SC1091
source "$HOME/.cargo/env"
rustup default 1.76.0
rustup target add thumbv7neon-linux-androideabi
rustup target add armv7-linux-androideabi
rustup target add aarch64-linux-android
rustup target add x86_64-linux-android
rustup target add i686-linux-android
cargo install --force --vers 0.26.0 cbindgen

# Build LLVM
pushd "$llvm"
llvmtarget=$(cat "$llvm/targets_to_build")
cmake -S llvm -B build -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=out -DCMAKE_C_COMPILER=clang-18 \
    -DCMAKE_CXX_COMPILER=clang++-18 -DLLVM_ENABLE_PROJECTS="clang" -DLLVM_TARGETS_TO_BUILD="$llvmtarget" \
    -DLLVM_USE_LINKER=lld -DLLVM_BINUTILS_INCDIR=/usr/include -DLLVM_ENABLE_PLUGINS=FORCE_ON \
    -DLLVM_DEFAULT_TARGET_TRIPLE="x86_64-unknown-linux-gnu"
cmake --build build -j"$(nproc)"
cmake --build build --target install -j"$(nproc)"
popd

# Build WASI SDK
pushd "$wasi"
mkdir -p build/install/wasi
touch build/compiler-rt.BUILT # fool the build system
make \
    PREFIX=/wasi \
    build/wasi-libc.BUILT \
    build/libcxx.BUILT \
    -j"$(nproc)"
popd

# Build microG libraries
pushd "$gmscore"
gradle -x javaDocReleaseGeneration \
    :play-services-ads-identifier:publishToMavenLocal \
    :play-services-base:publishToMavenLocal \
    :play-services-basement:publishToMavenLocal \
    :play-services-fido:publishToMavenLocal \
    :play-services-tasks:publishToMavenLocal
popd

pushd "$mozilla_release"
ABIS=(armeabi-v7a arm64-v8a x86 x86_64)
MOZ_CHROME_MULTILOCALE=$(< "$patches/locales")
export MOZ_CHROME_MULTILOCALE
export MOZILLA_OFFICIAL=1
MOZ_FETCHES_DIR=${mozilla_release}/fetches
mkdir -p "${MOZ_FETCHES_DIR}"

# TODO: check if all of these exports are needed
export MOZ_APP_VERSION=$1
export MOZ_BUILDID=${MOZ_APP_VERSION##*.}
export MOZ_APP_BUILDID=$MOZ_BUILDID
export MOZ_BUILD_DATE=$MOZ_BUILDID
echo "#define MOZ_BUILDID $MOZ_BUILDID" > buildid.h

# Set values for sonatype publishing
export GROUP_ID=${GROUP_ID:-"ie.equalit.ouinet"}
export OSSRH_USERNAME="${OSSRH_USERNAME}"
export OSSRH_PASSWORD="${OSSRH_PASSWORD}"
export SIGNING_PASSWORD="${SIGNING_PASSWORD}"
export SIGNING_KEY_ID="${SIGNING_KEY_ID}"
export SIGNING_KEY="${SIGNING_KEY}"

# Build once, allowing for failure, so we can build+publish exoplayer
./mach --verbose build || true
./mach gradle exoplayer2:publishReleasePublicationToMavenLocal
./mach gradle exoplayer2:publishReleaseToSonatype closeSonatypeStagingRepository
for ABI in ${ABIS[@]}; do
    case "$ABI" in
      armeabi-v7a)
        TARGET=arm-linux-androideabi
        ;;
      arm64-v8a)
        TARGET=aarch64-linux-android
        ;;
      x86_64)
        TARGET=x86_64-linux-android
        ;;
      x86)
        TARGET=i686-linux-android
        ;;
      *)
        echo "Unknown ABI: '$ABI', valid values are armeabi-v7a, arm64-v8a, x86 and x86_64."
        exit 1
    esac
    # Modify the mozconfig for the new target
    sed -i \
        -e "s/--target=.*/--target=$TARGET/" \
        -e "s/MOZ_OBJDIR=@TOPSRCDIR@\/obj-.*/MOZ_OBJDIR=@TOPSRCDIR@\/obj-$TARGET/" \
        mozconfig
    # Build for new target and publish
    ./mach --verbose build
    ./mach gradle geckoview:publishReleasePublicationToMavenLocal
    ./mach gradle geckoview:publishReleaseToSonatype closeSonatypeStagingRepository

    # Copy resulting AAR to fetches dir for fat AAR omni build
    AAR_OUTPUT_DIR=${HOME}/.m2/repository/${GROUP_ID//.//}/geckoview-ceno-omni-${ABI}/$1
    cp -f "${AAR_OUTPUT_DIR}"/*.aar ${MOZ_FETCHES_DIR}/.
done
# Set the AARs to be included in omni build
export MOZ_FETCHES_DIR
export MOZ_ANDROID_FAT_AAR_ARCHITECTURES="armeabi-v7a,arm64-v8a,x86,x86_64"
export MOZ_ANDROID_FAT_AAR_ARM64_V8A=geckoview-ceno-omni-arm64-v8a-$1.aar
export MOZ_ANDROID_FAT_AAR_ARMEABI_V7A=geckoview-ceno-omni-armeabi-v7a-$1.aar
export MOZ_ANDROID_FAT_AAR_X86=geckoview-ceno-omni-x86-$1.aar
export MOZ_ANDROID_FAT_AAR_X86_64=geckoview-ceno-omni-x86_64-$1.aar
sed -i \
    -e "s/--target=.*/--target=aarch64-linux-android/" \
    -e "s/MOZ_OBJDIR=@TOPSRCDIR@\/obj-.*/MOZ_OBJDIR=@TOPSRCDIR@\/obj-omni/" \
    mozconfig

 # Build omni AAR and publish
./mach --verbose build
./mach gradle geckoview:publishReleasePublicationToMavenLocal
./mach gradle geckoview:publishReleaseToSonatype closeSonatypeStagingRepository

# Make a bare-minimum mozconfig to start android-components builds
rm mozconfig
cat << EOF > mozconfig
mk_add_options AUTOCLOBBER=1
ac_add_options --enable-application=mobile/android
ac_add_options --target=x86_64
ac_add_options --enable-artifact-builds
mk_add_options MOZ_OBJDIR="./objdir-android-artifact"

ac_add_options --with-android-sdk="$ANDROID_SDK"
ac_add_options --with-java-bin-path="/usr/bin"
ac_add_options --with-gradle=$(command -v gradle)
ac_add_options --enable-release
ac_add_options --enable-update-channel=ceno
ac_add_options MOZILLA_OFFICIAL=1
EOF

# Build once, but allow for failure
# this is just to let the environment get built
./mach --verbose build || true

gradle :browser-engine-gecko:publishReleasePublicationToMavenLocal
gradle :browser-engine-gecko:publishReleaseToSonatype closeSonatypeStagingRepository
gradle :feature-toolbar:publishReleasePublicationToMavenLocal
gradle :feature-toolbar:publishReleaseToSonatype closeSonatypeStagingRepository
popd
