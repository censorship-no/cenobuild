FROM debian:bookworm
ENV LANG=C.UTF-8

RUN \
  apt update && apt install -y -t bookworm \
    androguard \
    autoconf \
    automake \
    autopoint \
    bc \
    binutils-dev \
    build-essential \
    bzip2 \
    ca-certificates-java \
    curl \
    cmake \
    gettext \
    git \
    gyp \
    g++ \
    lib32gcc-s1 \
    libffi-dev \
    libssl-dev \
    libsqlite3-dev \
    libtool \
    libz-dev \
    llvm-14 \
    m4 \
    make \
    mercurial \
    nasm \
    ninja-build \
    nodejs \
    npm \
    openjdk-17-jdk-headless \
    pkg-config \
    python-is-python3 \
    python3-defusedxml \
    python3-distutils \
    python3-git \
    python3-pip \
    python3-pyasn1 \
    python3-pyasn1-modules \
    python3-qrcode \
    python3-ruamel.yaml \
    python3-venv \
    rsync \
    sdkmanager \
    software-properties-common \
    sudo \
    tcl \
    texinfo \
    unzip \
    wget \
    zlib1g-dev \
    zstd

RUN \
  echo "deb https://deb.debian.org/debian trixie main" > /etc/apt/sources.list.d/trixie.list && \
  apt-get update && apt-get install -y -t trixie \
    clang-18 \
    llvm-18 \
    lld-18 \
    libssl-dev

# quieten wget and unzip
RUN echo 'quiet = on' >> /etc/wgetrc

RUN \
  echo "[extensions]" >> /etc/mercurial/hgrc && \
  echo "purge =" >> /etc/mercurial/hgrc

RUN \
  adduser --disabled-password --gecos "" fdroid && \
  echo "fdroid ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers

USER fdroid

ENV USER_HOME=/home/fdroid
ENV ANDROID_HOME=$USER_HOME/android-sdk-linux
ENV CMDLINE_VER=commandlinetools-linux-6858069_latest
ENV SDKMAN=${ANDROID_HOME}/cmdline-tools/bin/sdkmanager
ENV FDROIDSERV=$USER_HOME/fdroidserver
ENV FDROIDDATA=$USER_HOME/fdroiddata
ENV CENOBUILD=$USER_HOME/cenobuild
ENV PATH=$PATH:${ANDROID_HOME}/tools:${ANDROID_HOME}/platform-tools:${FDROIDSERV}

WORKDIR $USER_HOME

RUN \
    mkdir .gradle && \
    echo "org.gradle.daemon=false" > .gradle/gradle.properties

RUN \
    mkdir $ANDROID_HOME && \
    wget https://dl.google.com/android/repository/${CMDLINE_VER}.zip && \
    unzip ${CMDLINE_VER}.zip -d${ANDROID_HOME}/

RUN \
    git clone -b 2.2.2 https://gitlab.com/fdroid/fdroidserver.git ${FDROIDSERV} && \
    # Cherry pick gradlew-fdroid from master to fix missing gradle versions
    git -C ${FDROIDSERV} checkout origin/master -- gradlew-fdroid && \
    git clone https://gitlab.com/fdroid/fdroiddata.git ${FDROIDDATA}

RUN \
    yes y | ${SDKMAN} --sdk_root="${ANDROID_HOME}" platform-tools "build-tools;35.0.0" && \
    yes y | ${SDKMAN} --sdk_root="${ANDROID_HOME}"  --install "ndk;25.2.9519653" && \
    yes y | ${SDKMAN} --sdk_root="${ANDROID_HOME}"  --install "ndk;27.2.12479018" && \
    yes y | ${SDKMAN} --sdk_root="${ANDROID_HOME}"  --licenses

RUN \
    ln -s ${FDROIDSERV}/gradlew-fdroid ${FDROIDSERV}/gradle && \
    echo "sdk_path: ${ANDROID_HOME}" > ${FDROIDDATA}/config.yml

WORKDIR $USER_HOME/fdroiddata

SHELL ["/bin/bash", "-c"]
ENTRYPOINT \
  ${CENOBUILD}/entrypoint.sh
